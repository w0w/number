package com.paxplay.poptiles.manager;



/**
 * @author Rohit Goyal
 * @author PaxPlay
 * @version 1.0
 */
 
import java.io.IOException;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.graphics.Color;

import com.paxplay.poptiles.GameActivity;
import com.paxplay.poptiles.camera.FollowCamera;
 
 
public class ResourcesManager {
        private static final ResourcesManager INSTANCE = new ResourcesManager();
        public Engine engine;
        public GameActivity activity;
        public FollowCamera camera;
        public VertexBufferObjectManager vbom;
        public Font whiteFont;
        public Font blackFont;
        
        //Add game sounds and music
        public Sound scoreSound;
        public Sound[] tapSounds = new Sound[26]; 
        
        /* Example 
        
        public Sound jumpSound;
        public Sound dieSound;
        public Music playerMusic;
        
         End Example */
       
        public static ResourcesManager getInstance() {
                return INSTANCE;
        }
       
        //Splash Scene
        public ITextureRegion splash_region;
        //Play Button
        public ITextureRegion playbutton_region;
        public ITextureRegion leaderboard_region;
        public ITextureRegion restart_region;
        public ITextureRegion share_region;
        public ITextureRegion sound_on_region;
        public ITextureRegion sound_off_region;
        public ITextureRegion rate_region;
        public ITextureRegion leaderboard_home_region;
        //Sound Control
        public ITextureRegion musicOnRegion;
        
        //Parallax Region (Optional)
        
        /*Example for parallax */
        
        //public ITextureRegion parallaxBackLayerRegion;
        //public ITextureRegion parallaxMidLayerRegion;
        //public ITextureRegion parallaxFrontLayerRegion;
        
        /*End Example for parallax */
        
        // Normal Texture region //
        
        /*Example*/
        
        //public TextureRegion playerRegion;
        
        /*End Example */
        
        //ITexture region (Used for Simple Background)
        
        public ITextureRegion menuBackgroundRegion;
       
        //Define Atlas (Optimise Performance )
        
//        private BuildableBitmapTextureAtlas menuTextureAtlas;        
        private BitmapTextureAtlas splashTextureAtlas;
        
        private BuildableBitmapTextureAtlas gameTextureAtlas;
        //private BitmapTextureAtlas repeatingGroundAtlas;
 
 
       // Will comment later
        
        public static void prepareManager(Engine engine, GameActivity activity, FollowCamera camera, VertexBufferObjectManager vbom) {
                getInstance().engine = engine;
                getInstance().activity = activity;
                getInstance().camera = camera;
                getInstance().vbom = vbom;
        }
       
        // Loading Resources for menu
        
        public void loadMenuResources() {
                loadMenuGraphic();
                loadMenuSound();
                loadMenuFonts();
               
        }
        
       //loading fonts
        
        private void loadMenuFonts() {
                FontFactory.setAssetBasePath("font/");
                final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//                blackFont = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "font.ttf", 50, true, Color.BLACK, 2, Color.WHITE);
                whiteFont = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "font.ttf", 40*GameActivity.density, true, Color.WHITE, 0.4f*GameActivity.density, Color.BLACK);
//                blackFont.load();
                whiteFont.load();
        }
 
        private void loadMenuSound() {
                // TODO Auto-generated method stub
               
        }
       
        //Loads Game Sounds and Catch Exception in case of error(Imp)
        
        private void loadGameSound() {
        	
            try {
            	tapSounds[0] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundA.mp3");
            	tapSounds[1] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundB.mp3");
            	tapSounds[2] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundC.mp3");
            	tapSounds[3] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundD.mp3");
            	tapSounds[4] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundE.mp3");
            	tapSounds[5] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundF.mp3");
            	tapSounds[6] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundG.mp3");
            	tapSounds[7] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundH.mp3");
            	tapSounds[8] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundI.mp3");
            	tapSounds[9] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundJ.mp3");
            	tapSounds[10] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundK.mp3");
            	tapSounds[11] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundL.mp3");
            	tapSounds[12] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundM.mp3");
            	tapSounds[13] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundN.mp3");
            	tapSounds[14] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundO.mp3");
            	tapSounds[15] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundP.mp3");
            	tapSounds[16] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundQ.mp3");
            	tapSounds[17] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundR.mp3");
            	tapSounds[18] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundS.mp3");
            	tapSounds[19] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundT.mp3");
            	tapSounds[20] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundU.mp3");
            	tapSounds[21] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundV.mp3");
            	tapSounds[22] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundW.mp3");
            	tapSounds[23] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundX.mp3");
            	tapSounds[24] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundY.mp3");
            	tapSounds[25] = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundZ.mp3");
            	scoreSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/piano/soundZ.mp3");
            	
                    //jumpSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/jumpSound.wav");
                    //dieSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/dieSound.wav");
                    //playerMusic = MusicFactory.createMusicFromAsset(activity.getMusicManager(), activity, "sfx/playerMusic.mp3");
            } catch (IOException e) {
                    e.printStackTrace();
            }
        }
 
        private void loadMenuGraphic() {
//                BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
//                menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
//                playbutton_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "playbutton.png");
//                menuBackgroundRegion =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "menuBackground.png");
//        try
//        {
//                        this.menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
//                        this.menuTextureAtlas.load();
//                }
//        catch (final TextureAtlasBuilderException e)
//        {
//                        Debug.e(e);
//                }
//       
//        //loadGameSound();
        }
 
        public void loadGameResources() {
                BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
               
                //repeatingGroundAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
                //parallaxFrontLayerRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(repeatingGroundAtlas, activity, "parallax_bamboo_layer.png", 0, 0);
                //repeatingGroundAtlas.load();
               
                gameTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
                leaderboard_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "leaderboard.png");
                restart_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "restart.png");
                share_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "share.png");
                sound_off_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "sound_off.png");
                sound_on_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "sound_on.png");
                rate_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "rate.png");
                leaderboard_home_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "leaderboard_home.png");
                
               // musicOnRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "musicOn.png");
               
                /* Try to load game texture and catch exception on faliure */
                
                try
                {
                       //Load Game Texture..
                	
                		 this.gameTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
                        this.gameTextureAtlas.load();
                }
                catch (final TextureAtlasBuilderException e)
                {
                        Debug.e(e);
                }
               
               
                loadGameSound();
                loadMenuFonts();
               
        }
       
        public void loadSplashResources() {
                BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
                splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 300, 300, TextureOptions.BILINEAR);
                splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png",0,0);
                splashTextureAtlas.load();
        }
 
        public void unloadSplashScreen() {
                splashTextureAtlas.unload();
                splash_region = null;          
        }
       
        public void unloadMenuTexture() {
//                menuTextureAtlas.unload();     
        }
 
        public void loadMenuTextures() {
//                menuTextureAtlas.load();
               
        }
 
        public void unloadGameTextures() {
             //Unload Game Textures used   
        	//gameTextureAtlas.unload();
               
        }
       
       
 
}