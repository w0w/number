package com.paxplay.poptiles.manager;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;

//import com.paxplay.pinchit.scene.MainMenuScene;
import com.paxplay.poptiles.scene.BaseScene;
import com.paxplay.poptiles.scene.GameScene;
import com.paxplay.poptiles.scene.LoadingScene;
import com.paxplay.poptiles.scene.SplashScene;
 
 
 
public class SceneManager {
        public BaseScene splashScene;
        public BaseScene menuScene;
        public BaseScene loadingScene;
        public BaseScene gameScene;
       
        private static final SceneManager INSTANCE = new SceneManager();
       
        private SceneType currentSceneType;
        private BaseScene currentScene;
        private Engine engine = ResourcesManager.getInstance().engine;
       
       
        public enum SceneType{
                SCENE_SPLASH,
                SCENE_MENU,
                SCENE_LOADING,
                SCENE_GAME
        }
       
        public static SceneManager getInstance() {
                return INSTANCE;
        }
       
        public void setScene(BaseScene baseScene) {
                engine.setScene(baseScene);
                currentScene = baseScene;
                currentSceneType = baseScene.getSceneType();
        }
       
        public void setScene(SceneType sceneType) {
                switch (sceneType) {
                case SCENE_SPLASH:
                        setScene(splashScene);
                        break;
                case SCENE_MENU:
                        setScene (menuScene);
                case SCENE_LOADING:
                        setScene(loadingScene);
                case SCENE_GAME:
                        setScene(gameScene);
                default:
                        break;
                }
        }
       
        public void createSplashScene(OnCreateSceneCallback pOnCreateSceneCallback) {
                ResourcesManager.getInstance().loadSplashResources();
                splashScene = new SplashScene();
                currentScene = splashScene;
                pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
        }
       
        public void disposeSplashScene() {
                ResourcesManager.getInstance().unloadSplashScreen();
                splashScene.disposeScene();
                splashScene = null;
        }
 
        public void createMenuScene() {
                ResourcesManager.getInstance().loadMenuResources();
//                menuScene = new MainMenuScene();
                loadingScene = new LoadingScene();
                SceneManager.getInstance().setScene(menuScene);
                disposeSplashScene();
        }
       public void createGameScene(){
    	   ResourcesManager.getInstance().loadGameResources();
           gameScene = new GameScene();
           loadingScene = new LoadingScene();
           SceneManager.getInstance().setScene(gameScene);
           disposeSplashScene();
       }
        public BaseScene getCurrentScene() {
                return currentScene;
        }
 
        public void loadGameScene(final Engine mEngine) {
                setScene(loadingScene);
                ResourcesManager.getInstance().unloadMenuTexture();
                mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() {
                       
                        @Override
                        public void onTimePassed(TimerHandler pTimerHandler) {
                                mEngine.unregisterUpdateHandler(pTimerHandler);
                                ResourcesManager.getInstance().loadGameResources();
                                gameScene = new GameScene();
                                setScene(gameScene);
                        }
                }));
        }
 
        public void loadMenuScene(final Engine mEngine) {
                setScene(loadingScene);
                gameScene.disposeScene();
                ResourcesManager.getInstance().unloadGameTextures();
                mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback()
                {
            public void onTimePassed(final TimerHandler pTimerHandler)
            {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                ResourcesManager.getInstance().loadMenuTextures();
                        setScene(menuScene);
            }
                }));
               
        }
}