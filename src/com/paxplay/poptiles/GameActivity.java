package com.paxplay.poptiles;
     
import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
//import com.flurry.android.FlurryAgent;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.paxplay.poptiles.camera.FollowCamera;
import com.paxplay.poptiles.manager.ResourcesManager;
import com.paxplay.poptiles.manager.SceneManager;

     
     
    public class GameActivity extends BaseGameActivity {
     
            public static float CAMERA_WIDTH=0;
            public static float CAMERA_HEIGHT=0;
            private FollowCamera camera;
           
            SharedPreferences prefs;
            public SharedPreferences.Editor editor;
            
            public static GameHelper mHelper;
            public static EasyTracker easyTracker;
           
            public void setHighScore(int score) {
	            SharedPreferences.Editor settingsEditor = prefs.edit();
	            settingsEditor.putInt(Constants.KEY_HISCORE, score);
	            settingsEditor.commit();
            }
           
            public int getHighScore() {
                    return prefs.getInt(Constants.KEY_HISCORE, 0);
            }      
     
            @Override
            public Engine onCreateEngine(EngineOptions pEngineOptions)
            {
                    return new LimitedFPSEngine(pEngineOptions, 60);
            }
           
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event)
            {  
                if (keyCode == KeyEvent.KEYCODE_BACK)
                {
                    SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
                }
                return false;
            }
           
            @Override
            public EngineOptions onCreateEngineOptions() {
            		ScreenDimentions();
                    prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    camera = new FollowCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
                    EngineOptions engineOption = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.camera);
                    engineOption.getAudioOptions().setNeedsMusic(true);
                    engineOption.getAudioOptions().setNeedsSound(true);
                    engineOption.getRenderOptions().getConfigChooserOptions().setRequestedMultiSampling(true);
                    engineOption.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
                    engineOption.getTouchOptions().setNeedsMultiTouch(true);
                    return engineOption;
            }
     
            public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
                    ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
                    pOnCreateResourcesCallback.onCreateResourcesFinished();
            }
     
            @Override
            public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
                            throws IOException {
                    SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback);
            }
     
            @Override
            public void onPopulateScene(Scene pScene,
                            OnPopulateSceneCallback pOnPopulateSceneCallback)
                            throws IOException {
                    mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
                           
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                    mEngine.unregisterUpdateHandler(pTimerHandler);
                                    SceneManager.getInstance().createGameScene();
                            }
                    }));
                    pOnPopulateSceneCallback.onPopulateSceneFinished();
            }
           
//            @Override
            public synchronized void onResumeGame() {
                    super.onResumeGame();
                    if (SceneManager.getInstance().getCurrentScene() == SceneManager.getInstance().gameScene)
                    {
                    	SceneManager.getInstance().gameScene.setIgnoreUpdate(false);
                    }
//                    if(mHelper==null){
//                  	  mHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
////                  	  Log.d("gamehelper","mHelper was null on onResume so initialized again");
//                    }
//                    if (adView != null) {
//                        adView.resume();
//                    }
            }
     
//            @Override
            public synchronized void onPauseGame() {
                    super.onPauseGame();
                    if (SceneManager.getInstance().getCurrentScene() == SceneManager.getInstance().gameScene)
                    {
                    	SceneManager.getInstance().gameScene.setIgnoreUpdate(true);
                    }
//                    if (adView != null) {
//              	      adView.pause();
//                	}
            }
            
 
            
            public static float Screen_width_inch;
            public static float Screen_height_inch;
            public static float Screen_width_cm;
            public static float Screen_height_cm;
            public static int Screen_width_pixels;
            public static int Screen_height_pixels;
            public static float Screen_dpi;
            public static float Screen_size_inch;
            public static float density;
            public void ScreenDimentions(){
        	    final DisplayMetrics dm = new DisplayMetrics();
        	    getWindowManager().getDefaultDisplay().getMetrics(dm);
        	    Screen_dpi = dm.scaledDensity;
        	    density = dm.density;
        	    Screen_width_pixels = dm.widthPixels;
        	    Screen_height_pixels = dm.heightPixels;
        	    CAMERA_WIDTH = Screen_width_pixels;
        	    CAMERA_HEIGHT = Screen_height_pixels;
        	    Screen_width_inch = 1.00f*dm.widthPixels/(1.00f*dm.xdpi);
        	    Screen_height_inch = 1.00f*dm.heightPixels/(1.00f*dm.ydpi);
        	    Screen_width_cm = Screen_width_inch*2.56f;
        	    Screen_height_cm = Screen_height_inch*2.56f;
            }
          @Override
          public void onDestroy()
          {
                  super.onDestroy();
//                  if(adView!=null){
//                	  adView.destroy();
//                  }
          }
          @Override
          public void onResume() {
            super.onResume();
            if(mHelper==null){
          	  mHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
//          	  Log.d("gamehelper","mHelper was null on onResume so initialized again");
            }
            if (adView != null) {
                adView.resume();
            }
          }
          @Override
          public void onPause() {
          	super.onPause();
          	if (adView != null) {
          	      adView.pause();
      	    }
          }
            public void onCreate(Bundle savedInstanceState) {
            	super.onCreate(savedInstanceState);
                mHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
//                mHelper.enableDebugLog(true, "GameHelper");
                editor = prefs.edit();
                GameHelperListener listener = new GameHelper.GameHelperListener() {
                    @Override
                    public void onSignInSucceeded() {
                        // handle sign-in succeess
                    	Log.d("auth","sign in successful");
                    	editor.putBoolean("firstSignIn", true);
                		editor.commit();
                		if(prefs.getInt("autoSignIn", 0)==1){
                			easyTracker.send(MapBuilder
              				      .createEvent("social",     // Event category (required)
              				                   "auth",  // Event action (required)
              				                   "first_login",   // Event label
              				                   null)            // Event value
              				      .build()
              				);
//                			FlurryAgent.logEvent("first login");
                		}
                		easyTracker.send(MapBuilder
        				      .createEvent("social",     // Event category (required)
        				                   "auth",  // Event action (required)
        				                   "google_login",   // Event label
        				                   null)            // Event value
        				      .build()
        				);
//                		FlurryAgent.logEvent("google_login");
                    }
                    @Override
                    public void onSignInFailed() {
                        // handle sign-in failure (e.g. show Sign In button)
                    	Log.d("leaderboard","sign in failed");
                    }

                };
                mHelper.setup(listener);
                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId("ca-app-pub-4781262018973914/2907734183");
             // Create ad request.
                AdRequest intestitialAdRequest = new AdRequest.Builder().build();
             // Begin loading your interstitial.
                mInterstitialAd.loadAd(intestitialAdRequest);
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
//                        Toast.makeText(MyActivity.this,
//                                "The interstitial is loaded", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAdClosed() {
                        // Proceed to the next level.
//                        goToNextLevel();
                    	AdRequest intestitialAdRequest = new AdRequest.Builder().build();
                    	mInterstitialAd.loadAd(intestitialAdRequest);
                    }
                });
            }
            
            public static void displayInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                	mInterstitialAd.show();
                }
            }
            @Override
            protected void onStart() {
                super.onStart();
//                FlurryAgent.onStartSession(this, "Q49BY3XCQXYDV7J9PDQ8");
                EasyTracker.getInstance(this).activityStart(this);  // Google Analytics added
                easyTracker = EasyTracker.getInstance(this);
                if(prefs.getInt("autoSignIn", 0)==0){
                	mHelper.beginUserInitiatedSignIn();
//                	mHelper.onStart(this);
//                	Log.d("auth","auto sign successful");
                	editor.putInt("autoSignIn", 1);
            		editor.commit();
            		easyTracker.send(MapBuilder
        			      .createEvent("social",     // Event category (required)
        			                   "auth",  // Event action (required)
        			                   "autoSignIn_shown",   // Event label
        			                   null)            // Event value
        			      .build()
        			);
//            		FlurryAgent.logEvent("autoSignIn_shown");
                }
                if (prefs.getBoolean("firstSignIn", false)==true) {
////                    // auto sign in
                	if(!mHelper.isSignedIn()){
                		mHelper.getApiClient().connect();
//                		Log.d("auth","not siged in, sign in called onstart");
                	} else{
//                		Log.d("auth","Already signed in");
                	}
                }
//                mHelper.onStart(this);
//                Log.d("leaderboard","is connected "+mHelper.getApiClient().isConnected());
                
            }
            
            @Override
            protected void onStop() {
                super.onStop();
//                FlurryAgent.onEndSession(this);
//                mHelper.getApiClient().disconnect();
//                mHelper.onStop(); // removed to remove leaderboard crash 
//                Log.d("leaderboard","on stop completed");
                EasyTracker.getInstance(this).activityStop(this);  // Google analytics end code            
            }
            
            @Override
            protected void onActivityResult(int request, int response, Intent data) {
                super.onActivityResult(request, response, data);
                mHelper.onActivityResult(request, response, data);
            }
            
//          Banner ad through google api
            public static AdView adView;
            public static AdRequest adRequest;
            public static FrameLayout frameLayout;
            public static FrameLayout.LayoutParams adViewLayoutParams;
            public static InterstitialAd mInterstitialAd;
            
            @Override
            protected void onSetContentView() {
                
                frameLayout = new FrameLayout(this);
                final FrameLayout.LayoutParams frameLayoutLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
//                Log.d("Ads","frame layout params are "+frameLayoutLayoutParams.width+" and "+frameLayoutLayoutParams.height);
                adView = new AdView(this);
                new AdView(this);
                adView.setAdSize(AdSize.BANNER);
                
                adView.setAdUnitId("ca-app-pub-4781262018973914/1431000982");
//                Log.d("Ads","AdView banner size is "+adView.getWidth()+" and "+adView.getHeight());
                adViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
//                Log.d("Ads","ad layout params are "+adViewLayoutParams.width+" and "+adViewLayoutParams.height);
                this.mRenderSurfaceView = new RenderSurfaceView(this);
                mRenderSurfaceView.setRenderer(this.mEngine, this);
                
                adRequest = new AdRequest.Builder()
                .addTestDevice("499058361")
                .addTestDevice("SH27YTV03064")
                .build();
                
                adView.setAdListener(new AdListener() {
                	  @Override
                	  public void onAdLoaded() {
                		  frameLayout.removeView(adView);  // added to remove addview error
//                		  frameLayout.addView(mRenderSurfaceView, frameLayoutLayoutParams);
                		  frameLayout.addView(adView,adViewLayoutParams);
                	    // Save app state before going to the ad overlay.
                	  }
            	});
                
                final FrameLayout.LayoutParams surfaceViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                frameLayout.addView(this.mRenderSurfaceView,surfaceViewLayoutParams);
                this.setContentView(frameLayout, frameLayoutLayoutParams);
                adView.loadAd(adRequest);
            }


    }
     
