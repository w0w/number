package com.paxplay.poptiles.scene;

import java.util.Random;

//import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
//import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.games.Games;
import com.paxplay.poptiles.Constants;
import com.paxplay.poptiles.GameActivity;
import com.paxplay.poptiles.ourTile;
import com.paxplay.poptiles.manager.ResourcesManager;
import com.paxplay.poptiles.manager.SceneManager.SceneType;

public class GameScene extends BaseScene implements IOnSceneTouchListener {
	private static final int TIME_TO_RESSURECTION = 200;
	private HUD gameHUD;
	private Text scoreText;
	private Text tapToPlayText;
	private Text highScoreText;
	private Text yourScoreText;
	private Text numberText;
	private Text gameOverText;
	private boolean scored;
	private int nextNumber=1;
	private int score;
	int stopper = 0;
	long lastTextTime;
	long lastTextTap;
	ourTile[] rect;
	private int nextTouchNumber=1;
//	boolean isTapped = false;
	Random rand = new Random();
	
	Sprite restart_button;
	Sprite leaderboard_button;
	Sprite leaderboard_home_button;
	Sprite share_button;
	Sprite sound_on_button;
	Sprite sound_off_button;
	Sprite rate_button;

	Rectangle gameOverOverlay;
	enum STATE {
		NEW, PAUSED, PLAY, DEAD, AFTERLIFE;
	}

	long timestamp = 0;
	private STATE state = STATE.NEW;
	private STATE lastState;
	protected boolean isMute = false;
	private int restartVolly;
	private float phoneVolume;

	@Override
	public void createScene() {
		ResourcesManager.getInstance().loadGameResources();
		setOnSceneTouchListener(this);
		setTouchAreaBindingOnActionDownEnabled(true);
	    setTouchAreaBindingOnActionMoveEnabled(true);
		createBackground();    
		createHUD();
		CreateTiles();
		CreateGameOverObjects();
		StartScreen();
		
		score = 0;
		nextTouchNumber = 1;
		nextNumber=1;
		stopper = 0;
        Constants.counter = 0;
        lastTextTime = 0;
        
		Constants.gameSpeedConstant=Constants.initialgameSpeedConstant;
		try {

			activity.getHighScore();
		} catch (Exception e) {
			System.out.println("aa");
			activity.setHighScore(0);
		}
//		createNumber(nextNumber);
	}
	
	
	public void StartScreen(){
		leaderboard_home_button.setVisible(true);
		rate_button.setVisible(true);
		if(isMute){
			sound_off_button.setVisible(true);
			this.registerTouchArea(sound_off_button);
		} else {
			sound_on_button.setVisible(true);
			this.registerTouchArea(sound_on_button);
		}
		this.registerTouchArea(leaderboard_home_button);
		this.registerTouchArea(rate_button);
	}

	public void CreateTiles(){
		rect = new ourTile[Constants.rows_number*Constants.column_number];
		for(int i=0;i<Constants.rows_number*Constants.column_number;i++){
			rect[i] = this.makeColoredRectangle(GameActivity.CAMERA_WIDTH/8+(int)(i/5.0f)*GameActivity.CAMERA_WIDTH/Constants.rows_number,((i%5)+1.5f)*GameActivity.CAMERA_HEIGHT/(Constants.column_number+1), 0, 0, 0);
		}
		final Entity rectangleGroup = new Entity(0, 0);
		for(int i=0;i<Constants.rows_number*Constants.column_number;i++){
			rectangleGroup.attachChild(rect[i]);
		}
		this.attachChild(rectangleGroup);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);
		
		if(state==STATE.PLAY){
			
			if(System.currentTimeMillis()-lastTextTime>Constants.gameSpeedConstant){
	
				if(Constants.counter==20 && stopper==0)
				{
					Log.d("Game Over","Game over here 1");
					stopper=-1;
					state = STATE.DEAD;
					GameOver();
					
				}
				else if(stopper==0)
				{
					createNumber(nextNumber);	
				}
//				if((System.currentTimeMillis()-lastTextTap>2000)&&isTapped){
//					Log.d("Game Over","Game over here 2");
////					isTapped = false;
//					state = STATE.DEAD;
//					GameOver();
//				}


			}			

//			Constants.gameSpeedConstant+=1;
		}
	
			if (scored) {
			}

		// if first obstacle is out of the screen, delete it


		if (state == STATE.DEAD && timestamp + TIME_TO_RESSURECTION < System.currentTimeMillis()) {
			state = STATE.AFTERLIFE;
		}

	}

	private void createHUD() {             
		/*Sprite musicOn = new Sprite(420, 70, 70, 70, resourceManager.musicOnRegion, vbom) {
                        @Override
                        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                                //this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2, pSceneTouchEvent.getY() - this.getHeight() / 2);
                                if (pSceneTouchEvent.isActionUp())
                                {
                                	// Add code to mute
                                }
                                return true;
                        }
         	}; */

		gameHUD = new HUD();
		scoreText = new Text(20, 30, resourceManager.whiteFont, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setAnchorCenter(0, 0);
		
		scoreText.setText("Score: 0");

		tapToPlayText = new Text(GameActivity.CAMERA_WIDTH/2, GameActivity.CAMERA_HEIGHT/2, resourceManager.whiteFont, "TAP TO PLAY", new TextOptions(HorizontalAlign.CENTER), vbom);
//		tapToPlayText.setAnchorCenter(0, 0);
		
		tapToPlayText.setText("TAP TO PLAY");
		gameHUD.attachChild(tapToPlayText);
		gameHUD.attachChild(scoreText);
//		gameHUD.attachChild(highScoreText);
//		gameHUD.attachChild(yourScoreText);
//		scoreText.setColor(Color.WHITE);
//		tapToPlayText.setColor(Color.WHITE);
//		gameHUD.attachChild(gameOverText);
		// gameHUD.attachChild(musicOn);
		// gameHUD.registerTouchArea(musicOn);
		camera.setHUD(gameHUD);
//		highScoreText.setVisible(false);
//		yourScoreText.setVisible(false);
//		gameOverText.setVisible(false);
	}

	private void createBackground() {
		this.setBackground(new Background(Color.BLACK));
	}

//	@Override
	public void reset() {
//		super.reset();
		isBackPressed=false;
//		isTapped = false;
		nextTouchNumber = 1;
		nextNumber=1;
		stopper = 0;
        Constants.counter = 0;
        lastTextTime = 0;
		score = 0;
		Constants.gameSpeedConstant=Constants.initialgameSpeedConstant;
		tapToPlayText.setText("TAP TO PLAY");
		tapToPlayText.setVisible(true);

		//resourceManager.activity.setHighScore(0);
		yourScoreText.setText("0");
		yourScoreText.setVisible(false);
		highScoreText.setText("Best: " + activity.getHighScore());
		highScoreText.setVisible(false);
		
		
		for(int i=0;i<Constants.column_number*Constants.rows_number;i++){
			rect[i].detachChildren();
			rect[i].setColor(Color.BLACK);
			rect[i].isTilePresent=false;
			this.unregisterTouchArea(rect[i]);
		}
		state = STATE.NEW;
	}


	private boolean isBackPressed=false;
	@Override
	public void onBackKeyPressed() {
		if(state == STATE.NEW){
			if(isBackPressed){
				System.exit(0);
			} else{
				isBackPressed = true;
				activity.runOnUiThread(new Runnable(){
	        		@Override
	        	    public void run() {
	        			Toast.makeText(activity, "Press exit once more to exit the game",
	                            Toast.LENGTH_LONG).show();
	        		}
	        	});
			}
		}else if (state == STATE.AFTERLIFE){
			RestartGame();
		}
//		gameHUD.setVisible(false);
		//resourceManager.playerMusic.pause();
//		camera.clearUpdateHandlers();
//		SceneManager.getInstance().loadMenuScene(engine);
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub

	}
	
	public void StartGame(){
		isBackPressed=false;
		for(int i=0;i<rect.length;i++){
			this.registerTouchArea(rect[i]);
		}
		state = STATE.PLAY;
		Debug.d("->PLAY");
		scoreText.setText("Score: 0");
		tapToPlayText.setVisible(false);
		leaderboard_home_button.setVisible(false);
		rate_button.setVisible(false);
		sound_off_button.setVisible(false);
		sound_on_button.setVisible(false);
		
		this.unregisterTouchArea(leaderboard_home_button);
		this.unregisterTouchArea(rate_button);
		this.unregisterTouchArea(sound_off_button);
		this.unregisterTouchArea(sound_on_button);
		
		hideAd();
	}


	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		
		if (pSceneTouchEvent.isActionDown()) {
			if (state == STATE.PAUSED) {
				if (lastState != STATE.NEW) {

				}
				state = lastState;
				Debug.d("->" + state);
			} else if (state == STATE.NEW) {
				StartGame();

			} 
//			else if (state == STATE.DEAD) {
				//                  // don't touch the dead!
//			} 
//			else if (state == STATE.AFTERLIFE) {
//				reset();
//				state = STATE.NEW;
//				Debug.d("->NEW");
//				Log.d("game start","game has been started");				
//			} 
//			if(state != STATE.PAUSED && state != STATE.NEW && state != STATE.DEAD && state != STATE.AFTERLIFE) {
//
//			}
		} 
		if(pSceneTouchEvent.isActionMove()){
			
		}
		if(pSceneTouchEvent.isActionUp()){
			//            	
		}
		return false;
	}


	public void resume() {

	}

	public void pause() {
		//pause music
		//resourceManager.playerMusic.pause();           
	}      

	private ourTile makeColoredRectangle(final float pX, final float pY, final float pRed, final float pGreen, final float pBlue) {
		final ourTile coloredRect = new ourTile(pX, pY, (GameActivity.CAMERA_WIDTH/Constants.rows_number)-10, (GameActivity.CAMERA_HEIGHT/(Constants.column_number+1))-10, this.getVertexBufferObjectManager()){
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY)
			{
				if(state==STATE.PLAY){
					if (pSceneTouchEvent.isActionDown())
					{
//						Log.d("Tap","tile tapped");

//						System.out.println("text time.... "+lastTextTime);
						//						if(this.getZIndex()<0){
						if(this.isTilePresent){
//							if(!isTapped){
//								isTapped = true;
//							}
							lastTextTap = System.currentTimeMillis();
							
							this.setColor(Color.BLACK);
							this.setZIndex(0);
							this.isTilePresent=false;
							detachChildren();
							Constants.counter--;
							System.out.println("counter decreased"+Constants.counter);
							if(this.tileNumber == nextTouchNumber){
								pianoSound();
//								Log.d("game over","right tile number");
								score = nextTouchNumber;
								GameSpeedFunction();
								scoreText.setText("Score: "+score);
								nextTouchNumber++;
							} else{
								resourceManager.tapSounds[0].play();
//								Log.d("game over","wrong tile number");
//								Log.d("game over","nexttouchnumber is "+nextTouchNumber);
//								Log.d("game over","tile number is "+this.tileNumber);
								Log.d("Game Over","Game over here 3");
								GameOver();
								state = STATE.DEAD;
							}
						} else{
							resourceManager.tapSounds[0].play();
							Log.d("Game Over","Game over here 4");
							state = STATE.DEAD;
							GameOver();
//							pianooSound();
							//this.setColor(Color.WHITE);
							//isTileWhite = true;
						}
					}
					if (pSceneTouchEvent.isActionUp())
					{
//						Log.d("Tap","tile tapped");
//						this.setColor(Color.WHITE);
					}
				}
				return true;
			}
			@Override
			protected void onManagedUpdate(float pSecondsElapsed)
			{


				if (scored)
				{
					// Execute your actions.
				}
				super.onManagedUpdate(pSecondsElapsed);
			}
		};
		coloredRect.setColor(pRed, pGreen, pBlue);
		return coloredRect;

	}




	private VertexBufferObjectManager getVertexBufferObjectManager() {
		// TODO Auto-generated method stub
		return null;
	}
	public void createNumber(int numerical) {
		int max = Constants.column_number*Constants.rows_number;
		int min = 0;
		int j = min + (int)(Math.random() * ((max - min)));

//		if(rect[j].getZIndex() >= 0 )
		if(!rect[j].isTilePresent)
		{
			Constants.counter++;
			numberText = new Text(rect[j].getWidth()/2,rect[j].getHeight()/2,resourceManager.whiteFont,""+numerical,vbom);
			numberText.setText(""+numerical);
			lastTextTime = System.currentTimeMillis();
			System.out.println("last time " + lastTextTime);
			rect[j].attachChild(numberText);
			rect[j].setZIndex(-1);
			rect[j].isTilePresent=true;
			rect[j].setTag(j);
			rect[j].tileNumber = numerical;
			rect[j].setColor(Color.WHITE);
			numberText.setColor(Color.BLACK);
			nextNumber++;
		}else{
			createNumber(numerical);

		}
	}
	
	int soundNumber=0;
	private void pianoSound(){
		int a = (int)(Math.random() * ((25 - 0)));
		resourceManager.tapSounds[a].play();
//		soundNumber++;
		if(soundNumber>25){
			soundNumber=0;
		}
	}
	private void buttonSound(){
		resourceManager.tapSounds[15].play();
	}
	
	private boolean isRestartFlag=false;
	private boolean isLeaderboardFlag=false;
	private boolean isShareFlag=false;
	private boolean isRateFlag = false;
	private boolean isSoundButton = false;
	public void CreateGameOverObjects(){
		Log.d("screen management","CreateGameOverObjects function");
		leaderboard_button = new Sprite(GameActivity.CAMERA_WIDTH*0.7f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.leaderboard_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
	    				buttonSound();
	    	        	this.setScale(0.8f);
	    	        	isLeaderboardFlag = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isLeaderboardFlag){
	    				this.setScale(1.0f);
	    				isLeaderboardFlag = false;
	    				if (GameActivity.mHelper.isSignedIn()) {
        	        		activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(GameActivity.mHelper.getApiClient(), "CgkIpNCFyIQQEAIQBg"), 1);
        	        	} else{
        	        		GameActivity.mHelper.beginUserInitiatedSignIn();	
        	        	}
	    				GameActivity.easyTracker.send(MapBuilder
		      				      .createEvent("ui_action",     // Event category (required)
		      				                   "button_press",  // Event action (required)
		      				                   "leaderboard_endscreen_button",   // Event label
		      				                   null)            // Event value
		      				      .build()
	      				);
	    				// Open leaderboard
	    			}
    			}
				return true;
			}
		};
		
		leaderboard_home_button = new Sprite(GameActivity.CAMERA_WIDTH*0.7f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.leaderboard_home_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
	    				buttonSound();
	    	        	this.setScale(0.8f);
	    	        	isLeaderboardFlag = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isLeaderboardFlag){
	    				this.setScale(1.0f);
	    				isLeaderboardFlag = false;
	    				if (GameActivity.mHelper.isSignedIn()) {
        	        		activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(GameActivity.mHelper.getApiClient(), "CgkIpNCFyIQQEAIQBg"), 1);
//        	        		Log.d("leaderboard","already signed in so open leaderboard");
        	        	} else{
        	        		GameActivity.mHelper.beginUserInitiatedSignIn();
        	        	}
	    				GameActivity.easyTracker.send(MapBuilder
		      				      .createEvent("ui_action",     // Event category (required)
		      				                   "button_press",  // Event action (required)
		      				                   "leaderboard_homescreen_button",   // Event label
		      				                   null)            // Event value
		      				      .build()
	      				);
	    				// Open leaderboard
	    			}
    			}
				return true;
			}
		};
		restart_button = new Sprite(GameActivity.CAMERA_WIDTH*0.5f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.restart_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
	    			buttonSound();
	    	        	this.setScale(0.8f);
	    	        	isRestartFlag = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isRestartFlag){
	    				this.setScale(1.0f);
	    				restartVolly++;
	    				if((restartVolly-2)%4==0){
		    	        	activity.runOnUiThread(new Runnable(){
		    	        		@Override
		    	        		public void run(){
		    	        			GameActivity.displayInterstitial();
		    	        		}
		    	        	});
	    	        	}
	    				Log.d("Buttons","restart button");
	    				isRestartFlag = false;
	    				RestartGame();
	    				GameActivity.easyTracker.send(MapBuilder
		      				      .createEvent("ui_action",     // Event category (required)
		      				                   "button_press",  // Event action (required)
		      				                   "restart_button",   // Event label
		      				                   null)            // Event value
		      				      .build()
	      				);
//	    				GameOver();
	    				// Open leaderboard
	    			}
    			}
				return true;
			}
		};
		
		rate_button = new Sprite(GameActivity.CAMERA_WIDTH*0.5f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.rate_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
	    			buttonSound();
	    	        	this.setScale(0.8f);
	    	        	isRateFlag = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isRateFlag){
	    				this.setScale(1.0f);
	    				Log.d("Buttons","rate button");
	    				isRateFlag = false;
	    				RateApp();
	    				GameActivity.easyTracker.send(MapBuilder
		      				      .createEvent("ui_action",     // Event category (required)
		      				                   "button_press",  // Event action (required)
		      				                   "rate_button",   // Event label
		      				                   null)            // Event value
		      				      .build()
	      				);
	    			}
    			}
				return true;
			}
		};
		share_button = new Sprite(GameActivity.CAMERA_WIDTH*0.3f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.share_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
//	    	        	resourceManager.buttonSound.play();
	    	        	this.setScale(0.8f);
	    	        	isShareFlag = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isShareFlag){
	    				this.setScale(1.0f);
	    				ShareApp();
	    				GameActivity.easyTracker.send(MapBuilder
		      				      .createEvent("ui_action",     // Event category (required)
		      				                   "button_press",  // Event action (required)
		      				                   "share_button",   // Event label
		      				                   null)            // Event value
		      				      .build()
	      				);
	    			}
    			}
				return true;
			}
		};
		
		sound_off_button = new Sprite(GameActivity.CAMERA_WIDTH*0.3f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.sound_off_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
//	    	        	resourceManager.buttonSound.play();
	    	        	this.setScale(0.8f);
	    	        	isSoundButton = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isSoundButton){
	    				unregisterTouchArea(this);
	    				registerTouchArea(sound_on_button);
	    				this.setScale(1.0f);
	    				isSoundButton = false;
	    				isMute = false;
	    				this.setVisible(false);
	    				sound_on_button.setVisible(true);
	    				engine.getSoundManager().setMasterVolume(phoneVolume);
	    				// Mute unmute code
	    			}
    			}
				return true;
			}
		};
		
		sound_on_button = new Sprite(GameActivity.CAMERA_WIDTH*0.3f, GameActivity.CAMERA_HEIGHT*0.3f, GameActivity.CAMERA_WIDTH*0.2f,GameActivity.CAMERA_WIDTH*0.2f, resourceManager.sound_on_region, vbom){
			@Override
	        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
	    		if (pSceneTouchEvent.isActionDown())
	    	        {
//	    	        	resourceManager.buttonSound.play();
	    	        	this.setScale(0.8f);
	    	        	isSoundButton = true;
	    	        }
	    		if(pSceneTouchEvent.isActionUp()){
	    			if(isSoundButton){
	    				unregisterTouchArea(this);
	    				registerTouchArea(sound_off_button);
	    				this.setScale(1.0f);
	    				isMute = true;
	    				this.setVisible(false);
	    				sound_off_button.setVisible(true);
	    				isSoundButton = false;
	    				phoneVolume = engine.getSoundManager().getMasterVolume();
    	        		engine.getSoundManager().setMasterVolume(0);
	    				// Mute unmute code
	    			}
    			}
				return true;
			}
		};
		
		yourScoreText = new Text(GameActivity.CAMERA_WIDTH/2, 0.8f*GameActivity.CAMERA_HEIGHT, resourceManager.whiteFont, "Your Score: ", new TextOptions(HorizontalAlign.CENTER), vbom);
//		yourScoreText.setAnchorCenter(0, 0);
//		yourScoreText.setTextOptions(text);
		yourScoreText.setScale(2.0f);
		yourScoreText.setColor(Color.BLACK);
		yourScoreText.setText("");

		highScoreText = new Text(GameActivity.CAMERA_WIDTH/2, 0.7f*GameActivity.CAMERA_HEIGHT, resourceManager.whiteFont, "High Score: ", new TextOptions(HorizontalAlign.CENTER), vbom);
//		highScoreText.setAnchorCenter(0, 0);
		highScoreText.setScale(0.8f);
		highScoreText.setColor(Color.BLACK);
		highScoreText.setText("Best: 0");
		
		gameOverText= new Text(GameActivity.CAMERA_WIDTH/2, GameActivity.CAMERA_HEIGHT/2, resourceManager.whiteFont, "TAP TO PLAY", new TextOptions(HorizontalAlign.CENTER), vbom);
//		gameOverText.setAnchorCenter(0, 0);
		gameOverText.setColor(Color.BLACK);
//		gameOverText.set
		gameOverText.setText("GAME OVER");

		gameOverOverlay = new Rectangle(camera.getWidth()/2, camera.getHeight()/2, camera.getWidth(), camera.getHeight(), vbom);
		gameOverOverlay.setColor(1, 1, 1, 0.75f);
		attachChild(gameOverOverlay);
		attachChild(restart_button);
        attachChild(leaderboard_button);
        attachChild(leaderboard_home_button);
        attachChild(share_button);
        attachChild(rate_button);
        attachChild(sound_off_button);
        attachChild(sound_on_button);
        gameHUD.attachChild(gameOverText);
        gameHUD.attachChild(yourScoreText);
        gameHUD.attachChild(highScoreText);
        yourScoreText.setVisible(false);
        highScoreText.setVisible(false);
        restart_button.setVisible(false);
        leaderboard_button.setVisible(false);
        leaderboard_home_button.setVisible(false);
        share_button.setVisible(false);
        rate_button.setVisible(false);
        sound_off_button.setVisible(false);
        sound_on_button.setVisible(false);
        gameOverText.setVisible(false);
        gameOverOverlay.setVisible(false);
	}
	public void GameOver(){
		if (GameActivity.mHelper.isSignedIn()) {
			Games.Leaderboards.submitScore(GameActivity.mHelper.getApiClient(), "CgkIpNCFyIQQEAIQBg", score);
		} else{
			// nothing
		}
		for(int i=0;i<Constants.column_number*Constants.rows_number;i++){
			this.unregisterTouchArea(rect[i]);
		}
		if(activity.getHighScore()<score){
			activity.setHighScore(score);
			highScoreText.setText("New Best: "+activity.getHighScore());
		} else{
			highScoreText.setText("Best: "+activity.getHighScore());
		}
		yourScoreText.setText(""+score);		
		Log.d("scene management","game over function");
		restart_button.setVisible(true);
		leaderboard_button.setVisible(true);
		share_button.setVisible(true);
		gameOverText.setVisible(true);
		gameOverOverlay.setVisible(true);
		yourScoreText.setVisible(true);
		highScoreText.setVisible(true);
		
		this.registerTouchArea(restart_button);
        this.registerTouchArea(leaderboard_button);
        this.registerTouchArea(share_button);
        showAd();
	}

	
	public void RestartGame(){
		restart_button.setVisible(false);
		leaderboard_button.setVisible(false);
		share_button.setVisible(false);
		gameOverText.setVisible(false);
		gameOverOverlay.setVisible(false);
		yourScoreText.setVisible(false);
		highScoreText.setVisible(false);
		this.unregisterTouchArea(restart_button);
        this.unregisterTouchArea(leaderboard_button);
        this.unregisterTouchArea(share_button);
        score = 0;
        scoreText.setText("Score: "+score);
        reset();
		Log.d("game start","game has been started");
		StartScreen();
	}
	
	private void RateApp(){
	    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
	    browserIntent.setData(Uri.parse("market://details?id=com.paxplay.poptiles"));
	    if (MyStartActivity(browserIntent) == false) {
	    	browserIntent.setData(Uri.parse("http://play.google.com/store/apps/details?id=com.paxplay.poptiles"));
	    	if (MyStartActivity(browserIntent) == false) {
	    		// fuck off nothing can happen now
	    	}
	    }
    }
	private boolean MyStartActivity(Intent aIntent) {
        try
        {
            activity.startActivity(aIntent);
            return true;
        }
        catch (ActivityNotFoundException e)
        {
            return false;
        }
    }
	
	// Share button
    Intent sendIntent =new Intent();
    private void ShareApp(){
    	sendIntent.setAction(Intent.ACTION_SEND);
    	String extraText="";
		extraText = "Beat my score: "+score+" in #PopTiles"+" \n\n "+"http://play.google.com/store/apps/details?id=com.paxplay.poptiles";
    	sendIntent.putExtra(Intent.EXTRA_TEXT, extraText);
    	sendIntent.setType("text/plain");
    	activity.startActivity(sendIntent);
    }
    
    private void GameSpeedFunction(){
    	if(score%10==0 && Constants.gameSpeedConstant>Constants.minGameSpeedConstant){
    		Constants.gameSpeedConstant -= 20;
    	}
    }
    
    private void hideAd(){
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				GameActivity.adView.setEnabled(false);
				GameActivity.adView.setVisibility(View.GONE);
//				GameActivity.frameLayout.removeView(GameActivity.adView);
			}
		});
    }
    private void showAd(){
//    	final AdView adLayout = (AdView) findViewById(R.id.adView1);
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				GameActivity.adView.setVisibility(View.VISIBLE);
			}
		});
    }
}
