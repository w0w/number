package com.paxplay.poptiles.scene;
 
import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.color.Color;

import com.paxplay.poptiles.GameActivity;
import com.paxplay.poptiles.manager.SceneManager.SceneType;
 
public class SplashScene extends BaseScene {
       
        private static final float CAMERA_WIDTH = GameActivity.CAMERA_WIDTH;
        private static final float CAMERA_HEIGHT = GameActivity.CAMERA_HEIGHT;
       
        Sprite splash;
 
        @Override
        public void createScene() {
            this.getBackground().setColor(Color.BLACK);               
            splash = new Sprite(CAMERA_WIDTH/2, CAMERA_HEIGHT/2, resourceManager.splash_region, vbom){
                @Override
                protected void preDraw(GLState pGLState, Camera pCamera){
                	super.preDraw(pGLState, pCamera);
                	pGLState.enableDither();
                }
            };
//        	splash.setScale(1.5f);
            splash.setPosition(GameActivity.CAMERA_WIDTH/2, GameActivity.CAMERA_HEIGHT/2);
            float aspectRatio = splash.getHeight()/splash.getWidth();
            splash.setWidth(camera.getCameraSceneWidth()/2);
    		splash.setHeight((camera.getCameraSceneWidth()/2)*aspectRatio);
            attachChild(splash);
        }
 
        @Override
        public void onBackKeyPressed() {
                // TODO Auto-generated method stub
               
        }
 
        @Override
        public SceneType getSceneType() {
                return SceneType.SCENE_SPLASH;
        }
 
        @Override
        public void disposeScene() {
                splash.detachSelf();
                splash.dispose();
                this.detachSelf();
                this.dispose();               
        }
 
        @Override
        public void resume() {
                // TODO Auto-generated method stub
               
        }
 
        @Override
        public void pause() {
                // TODO Auto-generated method stub
               
        }
 
}